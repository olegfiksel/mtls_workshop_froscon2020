# Preparation for the mTLS workshop @ FrOSCon 2020

# Bootstrap your workstation

1. Direct internet connection without a forward proxy in the middle
2. Linux workstation or a VM
3. Docker installed (to run different curl versions)
4. netcat (`apt-get install -y netcat`)
5. curl compiled with openssl or gnutls support (not NSS) `curl --version`
6. OpenSSL (`apt-get install -y openssl`)
7. tcpdump (`apt-get install -y tcpdump`)
8. [Wireshark](https://www.wireshark.org/#download) with UI
9. [Certificates](./certs.tar.gz)

# Using the tools

* In the workshop we will be working mainly with `curl`, `openssl` and `Wireshark`. Please make sure you know how to use those tools. Basic knowledge is enough.

# tls basics

* In the workshop we will be debugging tls connections. Make sure you know how tcp and tls works in general.
  * TCP 3-way handshake
  * TLS and mTLS basic concepts
  * PKI concept
    * CA
    * Certificates
    * Private keys
    * Chain of trust

# Check if curl is working

```
curl \
	-v \
	--cacert ./certs/ca.crt \
	--cert ./certs/client.crt \
	--key ./certs/client.key \
	https://mtls-workshop.fiksel.info:13370
```

You should get a "Well done!" message.

# Questions?

* Contact me at
  * Matrix: #FrOSCon:fiksel.info
  * IRC: #froscon (Freenode)
  * Email: oleg_at_fiksel.info

# See you on the workshop! 🖐️